import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Importar el ContactComponent
import { ContactComponent } from './contact.component';

const routes: Routes = [
  // Agregar la ruta
  {
    path: '',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
