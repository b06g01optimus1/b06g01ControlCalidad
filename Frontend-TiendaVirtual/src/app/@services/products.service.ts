import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  // Atributos 
  // url: any = 'http://localhost:4000/apirest/'
  url: any = 'https://b06gxx-tiendavirtual.herokuapp.com/apirest/'

  constructor(private http: HttpClient) { }

  // Métodos o Funciones 

  // CRUD => Create
  guardarDatos(producto: any){
    return this.http.post(this.url, producto);
  }

  // CRUD => Read
  obtenerDatos(){
    return this.http.get(this.url);
  }

  // CRUD => Update
  actualizarDatos(id: any, producto: any){
    return this.http.put(this.url + id, producto);
  }

  // CRUD => Delete
  eliminarDatos(id: any){
    return this.http.delete(this.url + id)
  }
}
